<?php

namespace App\Http\Controllers\Api\V1;

use App\Drink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DrinksController extends Controller
{
    public function index()
    {
        return Drink::all();
    }

    public function show($id)
    {
        return Drink::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $drink = Drink::findOrFail($id);
        $drink->update($request->all());

        return $drink;
    }

    public function store(Request $request)
    {
        $drink = Drink::create($request->all());
        return $drink;
    }

    public function destroy($id)
    {
        $drink = Drink::findOrFail($id);
        $drink->delete();
        return '';
    }
}
