
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import DrinksIndex from './components/drinks/DrinksIndex.vue';
import DrinksCreate from './components/drinks/DrinksCreate.vue';
import DrinksEdit from './components/drinks/DrinksEdit.vue';

const routes = [
    {
        path: '/',
        components: {
            drinksIndex: DrinksIndex
        }
    },
    {path: '/admin/drinks/create', component: DrinksCreate, name: 'createDrink'},
    {path: '/admin/drinks/edit/:id', component: DrinksEdit, name: 'editDrink'},
]

const router = new VueRouter({ routes })

const app = new Vue({ router }).$mount('#app')
